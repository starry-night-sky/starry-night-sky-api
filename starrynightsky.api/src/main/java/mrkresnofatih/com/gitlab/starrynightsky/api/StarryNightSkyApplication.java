package mrkresnofatih.com.gitlab.starrynightsky.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarryNightSkyApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarryNightSkyApplication.class, args);
	}

}
