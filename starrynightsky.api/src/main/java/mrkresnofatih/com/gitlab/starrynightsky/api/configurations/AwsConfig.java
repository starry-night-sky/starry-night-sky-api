package mrkresnofatih.com.gitlab.starrynightsky.api.configurations;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.space.DynamoSpaceEntityImpl;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.DynamoUserEntityImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

@Configuration
public class AwsConfig {
    @Value("${starrynightsky.aws.access-key-id}")
    private String accessKeyId;

    @Value("${starrynightsky.aws.secret-access-key}")
    private String secretAccessKey;

    @Value("${starrynightsky.aws.region}")
    private String region;

    @Value("${starrynightsky.app.dynamodbtablename}")
    private String dynamoDbTableName;

    @Value("${starrynightsky.auth.secret}")
    private String authSecret;

    @Value("${starrynightsky.auth.issuer}")
    private String authIssuer;

    @Bean
    public StarryNightSkyConfig starryNightSkyConfig() {
        return new StarryNightSkyConfig(authSecret, authIssuer);
    }

    @Bean
    public DynamoDbClient dynamoDbClient() {
        return DynamoDbClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKeyId, secretAccessKey)))
                .region(Region.of(region))
                .build();
    }

    @Bean
    public DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable() {
        var db = DynamoDbEnhancedClient.builder()
                .dynamoDbClient(dynamoDbClient())
                .build();
        return db.table(dynamoDbTableName, TableSchema.fromBean(DynamoUserEntityImpl.class));
    }

    @Bean
    public DynamoDbTable<DynamoSpaceEntityImpl> dynamoSpaceEntityDynamoDbTable() {
        var db = DynamoDbEnhancedClient.builder()
                .dynamoDbClient(dynamoDbClient())
                .build();
        return db.table(dynamoDbTableName, TableSchema.fromBean(DynamoSpaceEntityImpl.class));
    }
}
