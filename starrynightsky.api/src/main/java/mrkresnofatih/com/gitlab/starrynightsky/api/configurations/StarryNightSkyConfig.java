package mrkresnofatih.com.gitlab.starrynightsky.api.configurations;

public class StarryNightSkyConfig {
    private String authSecret;
    private String authIssuer;

    public StarryNightSkyConfig() {
    }

    public StarryNightSkyConfig(String authSecret, String authIssuer) {
        this.authSecret = authSecret;
        this.authIssuer = authIssuer;
    }

    public String getAuthSecret() {
        return authSecret;
    }

    public void setAuthSecret(String authSecret) {
        this.authSecret = authSecret;
    }

    public String getAuthIssuer() {
        return authIssuer;
    }

    public void setAuthIssuer(String authIssuer) {
        this.authIssuer = authIssuer;
    }
}
