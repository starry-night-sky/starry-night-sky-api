package mrkresnofatih.com.gitlab.starrynightsky.api.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public FuncResponse<AuthLoginResponse> authLogin(@Valid @RequestBody AuthLoginRequest loginRequest) {
        return authService.authLogin(loginRequest);
    }

    @PostMapping("/signup")
    public FuncResponse<AuthSignupResponse> authSignup(@Valid @RequestBody AuthSignupRequest signupRequest) {
        return authService.authSignup(signupRequest);
    }
}
