package mrkresnofatih.com.gitlab.starrynightsky.api.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileGetResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileUpdateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.profile.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/profile")
public class ProfileController {
    private final ProfileService profileService;

    @Autowired
    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @PostMapping("/get")
    public FuncResponse<ProfileGetResponse> get(@Valid @RequestBody ProfileGetRequest getRequest) {
        return profileService.get(getRequest);
    }

    @PostMapping("/update")
    public FuncResponse<ProfileUpdateResponse> update(@Valid @RequestBody ProfileUpdateRequest updateRequest) {
        return profileService.update(updateRequest);
    }
}
