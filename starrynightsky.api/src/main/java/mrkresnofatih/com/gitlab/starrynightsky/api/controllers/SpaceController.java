package mrkresnofatih.com.gitlab.starrynightsky.api.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.space.*;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.space.SpaceService;
import mrkresnofatih.com.gitlab.starrynightsky.api.utilities.ApplicationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpaceController {

    private final SpaceService spaceService;

    @Autowired
    public SpaceController(SpaceService spaceService) {
        this.spaceService = spaceService;
    }

    @PostMapping("/api/v1/space/create")
    public FuncResponse<SpaceCreateResponse> createSpace(
            @RequestHeader(ApplicationConstants.UserJwtToken.UsernameClaim) String username,
            @Valid @RequestBody SpaceCreateRequest createRequest) {
        createRequest.setUsername(username);
        return spaceService.createSpace(createRequest);
    }

    @PostMapping("/api/v1/space/get")
    public FuncResponse<SpaceGetResponse> getSpace(
            @RequestHeader(ApplicationConstants.UserJwtToken.UsernameClaim) String username,
            @Valid @RequestBody SpaceGetRequest getRequest) {
        getRequest.setUsername(username);
        return spaceService.getSpace(getRequest);
    }

    @PostMapping("/api/v1/space/list")
    public FuncResponse<SpaceListResponse> listSpace(
            @RequestHeader(ApplicationConstants.UserJwtToken.UsernameClaim) String username,
            @Valid @RequestBody SpaceListRequest listRequest) {
        listRequest.setUsername(username);
        return spaceService.listSpace(listRequest);
    }

    @PostMapping("/api/v1/space/update")
    public FuncResponse<SpaceUpdateResponse> updateSpace(
            @RequestHeader(ApplicationConstants.UserJwtToken.UsernameClaim) String username,
            @Valid @RequestBody SpaceUpdateRequest updateRequest) {
        updateRequest.setUsername(username);
        return spaceService.updateSpace(updateRequest);
    }

    @PostMapping("/api/v1/space/delete")
    public FuncResponse<SpaceDeleteResponse> deleteSpace(
            @RequestHeader(ApplicationConstants.UserJwtToken.UsernameClaim) String username,
            @Valid @RequestBody SpaceDeleteRequest deleteRequest) {
        deleteRequest.setUsername(username);
        return spaceService.deleteSpace(deleteRequest);
    }
}
