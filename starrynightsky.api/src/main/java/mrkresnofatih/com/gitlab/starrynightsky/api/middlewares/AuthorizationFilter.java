package mrkresnofatih.com.gitlab.starrynightsky.api.middlewares;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtValidateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.jwt.JwtService;
import mrkresnofatih.com.gitlab.starrynightsky.api.utilities.ApplicationConstants;
import mrkresnofatih.com.gitlab.starrynightsky.api.utilities.MutableHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@Component
@Order(1)
public class AuthorizationFilter implements Filter {
    private final Logger logger;
    private final JwtService jwtService;

    @Autowired
    public AuthorizationFilter(JwtService jwtService) {
        this.jwtService = jwtService;
        logger = LoggerFactory.getLogger(AuthorizationFilter.class);
    }

    @Override
    public void doFilter(
            ServletRequest servletRequest,
            ServletResponse servletResponse,
            FilterChain filterChain) throws IOException, ServletException {
        var req = (HttpServletRequest) servletRequest;
        var uri = req.getRequestURI();
        var isAuthRequired = _isRequireAuth(uri);
        if (isAuthRequired) {
            logger.debug("IsAuthProtected API, Start validating the jwt");
            var token = req.getHeader("X-Access-Token");
            if (token != null && !token.isEmpty()) {
                logger.info("auth header is not null & not empty");
                var validationRequest = _getJwtValidationRequest();
                validationRequest.setToken(token);
                var jwtValidationResult = jwtService.validateJwt(validationRequest);
                if (!jwtValidationResult.isError() && jwtValidationResult.getData().isValid()) {
                    MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(req);
                    var claims = jwtValidationResult.getData().getClaims();
                    for (var claim : claims.keySet()) {
                        mutableRequest.putHeader(claim, claims.get(claim));
                    }
                    filterChain.doFilter(mutableRequest, servletResponse);
                    return;
                }
            }
            Map<String, Object> response = new HashMap<>();
            response.put("errorMessage", "Unauthorized");
            response.put("data", null);

            var httpServletResponse = (HttpServletResponse) servletResponse;
            httpServletResponse.setStatus(HttpStatus.FORBIDDEN.value());
            httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
            var mapper = new ObjectMapper();
            mapper.writeValue(httpServletResponse.getWriter(), response);
            return;
        }

        logger.debug("IsNotAuthProtected API, Will not validate the jwt");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private JwtValidateRequest _getJwtValidationRequest() {
        var validationRequest = new JwtValidateRequest();
        var targetClaims = new HashSet<String>();
        targetClaims.add(ApplicationConstants.UserJwtToken.AvatarClaim);
        targetClaims.add(ApplicationConstants.UserJwtToken.UsernameClaim);
        validationRequest.setClaims(targetClaims);
        return validationRequest;
    }

    private boolean _isRequireAuth(String path) {
        var prefixList = new ArrayList<String>();
        prefixList.add("/api/v1/profile");
        prefixList.add("/api/v1/space");

        for (var pref : prefixList) {
            if (path.startsWith(pref)) {
                return true;
            }
        }
        return false;
    }
}
