package mrkresnofatih.com.gitlab.starrynightsky.api.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FuncResponse<T> {
    private String errorMessage;
    private T data;

    public FuncResponse() {
    }

    public FuncResponse(T data) {
        this.data = data;
        this.errorMessage = "";
    }

    public FuncResponse(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public FuncResponse(String errorMessage, T data) {
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @JsonIgnore
    public boolean isError() {
        return !(this.errorMessage.isEmpty() || this.errorMessage.isBlank());
    }
}
