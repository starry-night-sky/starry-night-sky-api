package mrkresnofatih.com.gitlab.starrynightsky.api.models.auth;

public class AuthLoginResponse {
    private String accessToken;
    private String username;

    public AuthLoginResponse() {
    }

    public AuthLoginResponse(String accessToken, String username) {
        this.accessToken = accessToken;
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
