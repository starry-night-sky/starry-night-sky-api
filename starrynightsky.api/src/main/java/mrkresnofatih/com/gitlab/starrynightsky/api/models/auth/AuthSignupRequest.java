package mrkresnofatih.com.gitlab.starrynightsky.api.models.auth;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

public class AuthSignupRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;
    @NotNull
    @NotBlank
    private String avatar;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[^A-Za-z0-9]).{8,}$")
    private String password;

    public AuthSignupRequest() {
    }

    public AuthSignupRequest(String username, String avatar, String password) {
        this.username = username;
        this.avatar = avatar;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
