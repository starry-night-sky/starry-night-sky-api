package mrkresnofatih.com.gitlab.starrynightsky.api.models.auth;

public class AuthSignupResponse {
    private String message;
    public AuthSignupResponse() {
    }

    public AuthSignupResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
