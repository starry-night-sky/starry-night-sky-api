package mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

import java.util.Dictionary;
import java.util.Map;

public class JwtCreateRequest extends JsonSerializable {
    private Map<String, String> claims;

    private Long lifeTime;

    public JwtCreateRequest() {
    }

    public JwtCreateRequest(Map<String, String> claims, Long lifeTime) {
        this.claims = claims;
        this.lifeTime = lifeTime;
    }

    public Map<String, String> getClaims() {
        return claims;
    }

    public void setClaims(Map<String, String> claims) {
        this.claims = claims;
    }

    public Long getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(Long lifeTime) {
        this.lifeTime = lifeTime;
    }
}
