package mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

import java.util.Set;

public class JwtValidateRequest extends JsonSerializable {
    private Set<String> claims;
    private String token;

    public JwtValidateRequest() {
    }

    public JwtValidateRequest(Set<String> claims, String token) {
        this.claims = claims;
        this.token = token;
    }

    public Set<String> getClaims() {
        return claims;
    }

    public void setClaims(Set<String> claims) {
        this.claims = claims;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
