package mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt;

import java.util.Map;

public class JwtValidateResponse {
    private boolean valid;
    private Map<String, String> claims;

    public JwtValidateResponse() {
    }

    public JwtValidateResponse(boolean valid, Map<String, String> claims) {
        this.valid = valid;
        this.claims = claims;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Map<String, String> getClaims() {
        return claims;
    }

    public void setClaims(Map<String, String> claims) {
        this.claims = claims;
    }
}
