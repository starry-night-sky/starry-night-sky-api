package mrkresnofatih.com.gitlab.starrynightsky.api.models.profile;

public class ProfileGetResponse {
    private String username;
    private String avatar;

    public ProfileGetResponse() {
    }

    public ProfileGetResponse(String username, String avatar) {
        this.username = username;
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
