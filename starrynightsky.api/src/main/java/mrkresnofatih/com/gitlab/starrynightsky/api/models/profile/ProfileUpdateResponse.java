package mrkresnofatih.com.gitlab.starrynightsky.api.models.profile;

public class ProfileUpdateResponse {
    private String message;

    public ProfileUpdateResponse() {
    }

    public ProfileUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
