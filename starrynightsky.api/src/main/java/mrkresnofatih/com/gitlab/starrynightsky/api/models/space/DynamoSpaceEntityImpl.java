package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;

@DynamoDbBean
public class DynamoSpaceEntityImpl {
    private String partitionKey;
    private String sortKey;
    private String username;
    private String name;
    private String description;
    private String color;
    private String logo;

    public DynamoSpaceEntityImpl() {
    }

    public DynamoSpaceEntityImpl(String partitionKey, String sortKey, String username, String name, String description, String color, String logo) {
        this.partitionKey = partitionKey;
        this.sortKey = sortKey;
        this.username = username;
        this.name = name;
        this.description = description;
        this.color = color;
        this.logo = logo;
    }

    @DynamoDbPartitionKey
    public String getPartitionKey() {
        return partitionKey;
    }

    public void setPartitionKey(String partitionKey) {
        this.partitionKey = partitionKey;
    }

    @DynamoDbSortKey
    public String getSortKey() {
        return sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
