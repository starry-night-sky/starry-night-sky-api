package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

public class SpaceCreateResponse {
    private String message;

    public SpaceCreateResponse() {
    }

    public SpaceCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
