package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

public class SpaceDeleteResponse {
    private String message;

    public SpaceDeleteResponse() {
    }

    public SpaceDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
