package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

public class SpaceGetRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{1,30}$")
    private String name;

    public SpaceGetRequest() {
    }

    public SpaceGetRequest(String username, String name) {
        this.username = username;
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
