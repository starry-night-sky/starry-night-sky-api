package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

public class SpaceGetResponse {
    private String username;
    private String name;
    private String description;
    private String color;
    private String logo;

    public SpaceGetResponse() {
    }

    public SpaceGetResponse(String username, String name, String description, String color, String logo) {
        this.username = username;
        this.name = name;
        this.description = description;
        this.color = color;
        this.logo = logo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
