package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;
import org.hibernate.validator.constraints.Range;

public class SpaceListRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{1,30}$")
    private String name;
    @NotNull
    @Range(min = 10, max = 100)
    private long pageSize;

    public SpaceListRequest() {
    }

    public SpaceListRequest(String username, String name, long pageSize) {
        this.username = username;
        this.name = name;
        this.pageSize = pageSize;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }
}
