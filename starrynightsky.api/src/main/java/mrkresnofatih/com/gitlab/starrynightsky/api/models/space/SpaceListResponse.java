package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

import java.util.List;

public class SpaceListResponse {
    private List<SpaceGetResponse> results;

    public SpaceListResponse() {
    }

    public SpaceListResponse(List<SpaceGetResponse> results) {
        this.results = results;
    }

    public List<SpaceGetResponse> getResults() {
        return results;
    }

    public void setResults(List<SpaceGetResponse> results) {
        this.results = results;
    }
}
