package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

public class SpaceUpdateRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{1,30}$")
    private String name;
    @NotNull
    @NotBlank
    private String description;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$")
    private String color;
    @NotNull
    @NotBlank
    private String logo;

    public SpaceUpdateRequest() {
    }

    public SpaceUpdateRequest(String username, String name, String description, String color, String logo) {
        this.username = username;
        this.name = name;
        this.description = description;
        this.color = color;
        this.logo = logo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
