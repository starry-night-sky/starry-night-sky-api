package mrkresnofatih.com.gitlab.starrynightsky.api.models.space;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

public class SpaceUpdateResponse extends JsonSerializable {
    private String message;

    public SpaceUpdateResponse() {
    }

    public SpaceUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
