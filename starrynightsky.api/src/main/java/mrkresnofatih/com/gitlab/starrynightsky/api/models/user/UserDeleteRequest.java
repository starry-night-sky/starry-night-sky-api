package mrkresnofatih.com.gitlab.starrynightsky.api.models.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

public class UserDeleteRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    private String username;

    public UserDeleteRequest() {
    }

    public UserDeleteRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
