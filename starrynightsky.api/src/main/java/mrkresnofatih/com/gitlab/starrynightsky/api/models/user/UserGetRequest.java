package mrkresnofatih.com.gitlab.starrynightsky.api.models.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

public class UserGetRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    private String username;

    public UserGetRequest() {
    }

    public UserGetRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
