package mrkresnofatih.com.gitlab.starrynightsky.api.models.user;

public class UserGetResponse {
    private String username;
    private String avatar;
    private String password;

    public UserGetResponse() {
    }

    public UserGetResponse(String username, String avatar, String password) {
        this.username = username;
        this.avatar = avatar;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
