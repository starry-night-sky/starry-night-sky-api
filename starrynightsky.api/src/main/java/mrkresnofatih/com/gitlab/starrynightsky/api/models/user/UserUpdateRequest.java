package mrkresnofatih.com.gitlab.starrynightsky.api.models.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.JsonSerializable;

public class UserUpdateRequest extends JsonSerializable {
    @NotBlank
    @NotNull
    private String username;

    @NotBlank
    @NotNull
    private String avatar;

    @NotBlank
    @NotNull
    private String password;

    public UserUpdateRequest() {
    }

    public UserUpdateRequest(String username, String avatar, String password) {
        this.username = username;
        this.avatar = avatar;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
