package mrkresnofatih.com.gitlab.starrynightsky.api.services.auth;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupResponse;

public interface AuthService {
    FuncResponse<AuthLoginResponse> authLogin(AuthLoginRequest loginRequest);

    FuncResponse<AuthSignupResponse> authSignup(AuthSignupRequest signupRequest);
}
