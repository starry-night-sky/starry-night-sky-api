package mrkresnofatih.com.gitlab.starrynightsky.api.services.auth;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserCreateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserGetResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.jwt.JwtService;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.user.UserService;
import mrkresnofatih.com.gitlab.starrynightsky.api.utilities.ApplicationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Objects;

@Service
public class SimpleAuthServiceImpl implements AuthService {
    private final UserService userService;
    private final JwtService jwtService;
    private final Logger logger;

    @Autowired
    public SimpleAuthServiceImpl(UserService userService, JwtService jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
        this.logger = LoggerFactory.getLogger(SimpleAuthServiceImpl.class);
    }

    @Override
    public FuncResponse<AuthLoginResponse> authLogin(AuthLoginRequest loginRequest) {
        logger.info("Start SimpleAuthServiceImpl-Login w. data: {}", loginRequest.toJsonSerialized());
        var getUserResponse = userService.getUser(new UserGetRequest(loginRequest.getUsername()));
        if (getUserResponse.isError()) {
            logger.error(getUserResponse.getErrorMessage());
            return new FuncResponse<>("User Not Found");
        }
        logger.info("User Found!");
        var isPasswordValid = Objects.equals(getUserResponse.getData().getPassword(), loginRequest.getPassword());
        if (!isPasswordValid) {
            logger.info("Credentials Are Invalid!");
            return new FuncResponse<>("Invalid Credentials");
        }
        var createUserTokenResult = _createUserToken(getUserResponse.getData());
        if (createUserTokenResult.isError()) {
            logger.error("Failed to generate JWT Token");
            return new FuncResponse<>("Failed To Generate JWT Token");
        }
        var token = createUserTokenResult.getData().getToken();
        var loginResponse = new AuthLoginResponse(token, loginRequest.getUsername());
        logger.info("SimpleAuthServiceImpl-Login success");
        return new FuncResponse<>(loginResponse);
    }

    @Override
    public FuncResponse<AuthSignupResponse> authSignup(AuthSignupRequest signupRequest) {
        logger.info("Start SimpleAuthServiceImpl-Signup w. data: {}", signupRequest.toJsonSerialized());
        var getUserResponse = userService.getUser(new UserGetRequest(signupRequest.getUsername()));
        if (!getUserResponse.isError()) {
            logger.info("User Already Exists!");
            return new FuncResponse<>("User With That UserName Already Exists");
        }
        logger.info("UserName still available");
        var avatar = signupRequest.getAvatar() == null ? _GetUserDefaultAvatar() : signupRequest.getAvatar();
        var createUserResponse = userService.createUser(new UserCreateRequest(
                signupRequest.getUsername(),
                avatar,
                signupRequest.getPassword()
        ));
        if (createUserResponse.isError()) {
            logger.info("Failed To Create User");
            return new FuncResponse<>("Failed To Create User");
        }
        logger.info("SimpleAuthServiceImpl-Signup Success");
        var signUpResponse = new AuthSignupResponse("Signup Success!");
        return new FuncResponse<>(signUpResponse);
    }

    private FuncResponse<JwtCreateResponse> _createUserToken(UserGetResponse userGetResponse) {
        var claims = new HashMap<String, String>();
        claims.put(ApplicationConstants.UserJwtToken.UsernameClaim, userGetResponse.getUsername());
        claims.put(ApplicationConstants.UserJwtToken.AvatarClaim, userGetResponse.getAvatar());
        return jwtService.createJwt(new JwtCreateRequest(claims, 86400L));
    }

    private String _GetUserDefaultAvatar() {
        return "https://static.vecteezy.com/system/resources/previews/009/734/564/original/default-avatar-profile-icon-of-social-media-user-vector.jpg";
    }
}
