package mrkresnofatih.com.gitlab.starrynightsky.api.services.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import mrkresnofatih.com.gitlab.starrynightsky.api.configurations.StarryNightSkyConfig;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtValidateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtValidateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class Auth0JwtServiceImpl implements JwtService {
    private final String secret;

    private final String issuer;

    private final String EXPIRES_AT_CLAIM_KEY = "x-expire-time";
    private final String TOKEN_ISSUER_CLAIM_KEY = "x-token-issuer";

    private final Logger logger;

    @Autowired
    public Auth0JwtServiceImpl(StarryNightSkyConfig starryNightSkyConfig) {
        this.issuer = starryNightSkyConfig.getAuthIssuer();
        this.secret = starryNightSkyConfig.getAuthSecret();
        this.logger = LoggerFactory.getLogger(Auth0JwtServiceImpl.class);
    }

    @Override
    public FuncResponse<JwtCreateResponse> createJwt(JwtCreateRequest createRequest) {
        try {
            logger.info("Start createJwt w. data: {}", createRequest.toJsonSerialized());
            var token = JWT.create()
                    .withPayload(createRequest.getClaims())
                    .withClaim(TOKEN_ISSUER_CLAIM_KEY, issuer)
                    .withClaim(EXPIRES_AT_CLAIM_KEY, _getUtcExpiresAfter(createRequest.getLifeTime()))
                    .sign(_getAlgorithm());
            logger.info("success create jwt token");
            return new FuncResponse<>(new JwtCreateResponse(token));
        }
        catch (Exception e) {
            logger.error("Failed to createJwt with error: {}", e.getMessage());
            return new FuncResponse<>("CreateJwt Failed");
        }
    }

    @Override
    public FuncResponse<JwtValidateResponse> validateJwt(JwtValidateRequest validateRequest) {
        try {
            logger.info("Start validateJwt w. data: {}", validateRequest.toJsonSerialized());
            var verification = JWT.require(_getAlgorithm())
                    .withClaim(TOKEN_ISSUER_CLAIM_KEY, issuer)
                    .withClaimPresence(EXPIRES_AT_CLAIM_KEY);
            var jwtVerifier = verification.build();
            var decoded = jwtVerifier.verify(validateRequest.getToken());

            if (decoded.getClaim(EXPIRES_AT_CLAIM_KEY).asLong() < _getUtcNow()) {
                throw new Exception("expired_at claim key of token is expired");
            }

            var outputClaims = new HashMap<String, String>();
            for (var claimType : validateRequest.getClaims()) {
                if (!decoded.getClaim(claimType).isMissing() || !decoded.getClaim(claimType).isNull()) {
                    var claimValue = decoded.getClaim(claimType).asString();
                    outputClaims.put(claimType, claimValue);
                }
            }
            logger.info("Finish validateJwt");
            return new FuncResponse<>(new JwtValidateResponse(true, outputClaims));
        }
        catch (Exception e) {
            logger.error("Failed to validateJwt with error: {}", e.getMessage());
            return new FuncResponse<>("ValidateJwt Failed");
        }
    }

    private Algorithm _getAlgorithm() {
        return Algorithm.HMAC256(secret);
    }

    private Long _getUtcExpiresAfter(Long lifeTime) {
        return System.currentTimeMillis() + lifeTime * 1000;
    }

    private Long _getUtcNow() {
        return System.currentTimeMillis();
    }
}
