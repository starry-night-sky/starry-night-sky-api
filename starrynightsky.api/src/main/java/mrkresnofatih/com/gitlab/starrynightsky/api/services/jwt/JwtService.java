package mrkresnofatih.com.gitlab.starrynightsky.api.services.jwt;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtValidateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtValidateResponse;

public interface JwtService {
    FuncResponse<JwtCreateResponse> createJwt(JwtCreateRequest createRequest);

    FuncResponse<JwtValidateResponse> validateJwt(JwtValidateRequest validateRequest);
}
