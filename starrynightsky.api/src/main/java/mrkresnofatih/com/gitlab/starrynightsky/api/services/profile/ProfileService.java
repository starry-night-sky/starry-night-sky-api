package mrkresnofatih.com.gitlab.starrynightsky.api.services.profile;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileGetResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileUpdateResponse;

public interface ProfileService {
    FuncResponse<ProfileGetResponse> get(ProfileGetRequest getRequest);

    FuncResponse<ProfileUpdateResponse> update(ProfileUpdateRequest updateRequest);
}
