package mrkresnofatih.com.gitlab.starrynightsky.api.services.profile;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileGetResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileUpdateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserUpdateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimpleProfileServiceImpl implements ProfileService {
    private final UserService userService;
    private final Logger logger;

    @Autowired
    public SimpleProfileServiceImpl(UserService userService) {
        this.userService = userService;
        this.logger = LoggerFactory.getLogger(SimpleProfileServiceImpl.class);
    }

    @Override
    public FuncResponse<ProfileGetResponse> get(ProfileGetRequest getRequest) {
        logger.info("Start get profile w. data: {}", getRequest.toJsonSerialized());
        var getUserResult = userService
                .getUser(new UserGetRequest(getRequest.getUsername()));
        if (getUserResult.isError()) {
            logger.info("Failed to get user from username provided");
            return new FuncResponse<>("Failed to get user");
        }
        var userData = getUserResult.getData();
        return new FuncResponse<>(new ProfileGetResponse(userData.getUsername(), userData.getAvatar()));
    }

    @Override
    public FuncResponse<ProfileUpdateResponse> update(ProfileUpdateRequest updateRequest) {
        logger.info("Start update profile w. data: {}", updateRequest.toJsonSerialized());
        var updateUserResult = userService
                .updateUser(new UserUpdateRequest(
                        updateRequest.getUsername(),
                        updateRequest.getAvatar(),
                        updateRequest.getPassword()
                ));
        if (updateUserResult.isError()) {
            return new FuncResponse<>("Failed to update user");
        }
        return new FuncResponse<>(new ProfileUpdateResponse("Success update profile"));
    }
}
