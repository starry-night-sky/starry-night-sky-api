package mrkresnofatih.com.gitlab.starrynightsky.api.services.space;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.space.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;

import java.util.ArrayList;
import java.util.Objects;

@Service
public class DynamoSpaceServiceImpl implements SpaceService {
    private final Logger logger;
    private final DynamoDbTable<DynamoSpaceEntityImpl> dynamoSpaceServiceDynamoDbTable;

    @Autowired
    public DynamoSpaceServiceImpl(DynamoDbTable<DynamoSpaceEntityImpl> dynamoSpaceServiceDynamoDbTable) {
        this.logger = LoggerFactory.getLogger(DynamoSpaceServiceImpl.class);
        this.dynamoSpaceServiceDynamoDbTable = dynamoSpaceServiceDynamoDbTable;
    }

    @Override
    public FuncResponse<SpaceCreateResponse> createSpace(SpaceCreateRequest createRequest) {
        logger.info("Start DynamoSpaceServiceImpl-Create w. data: {}", createRequest.toJsonSerialized());
        try {
            var newSpace = new DynamoSpaceEntityImpl();
            newSpace.setUsername(createRequest.getUsername());
            newSpace.setName(createRequest.getName());
            newSpace.setDescription(createRequest.getDescription());
            newSpace.setColor(createRequest.getColor());
            newSpace.setLogo(createRequest.getLogo());
            newSpace.setPartitionKey(_GetSpacePartitionKey(createRequest.getUsername()));
            newSpace.setSortKey(_GetSpaceSortKey(createRequest.getUsername(), createRequest.getName()));

            dynamoSpaceServiceDynamoDbTable.putItem(newSpace);
            var newSpaceResponse = new SpaceCreateResponse("Space successfully Created");
            logger.info("DynamoSpaceServiceImpl-Create successful");
            return new FuncResponse<>(newSpaceResponse);
        }
        catch (Exception e) {
            logger.error("DynamoSpaceServiceImpl-Create failed: {}", e.getMessage());
            return new FuncResponse<>("DynamoSpaceServiceImpl-Create failed!");
        }
    }

    @Override
    public FuncResponse<SpaceGetResponse> getSpace(SpaceGetRequest getRequest) {
        logger.info("Start DynamoSpaceServiceImpl-Get w. data: {}", getRequest.toJsonSerialized());
        try {
            var partitionKey = _GetSpacePartitionKey(getRequest.getUsername());
            var sortKey = _GetSpaceSortKey(getRequest.getUsername(), getRequest.getName());
            var spaceKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var spaceFound = dynamoSpaceServiceDynamoDbTable.getItem(spaceKey);
            if (Objects.isNull(spaceFound)) {
                throw new Exception("space not found");
            }
            var spaceGetResponse = new SpaceGetResponse();
            spaceGetResponse.setUsername(spaceFound.getUsername());
            spaceGetResponse.setName(spaceFound.getName());
            spaceGetResponse.setColor(spaceFound.getColor());
            spaceGetResponse.setDescription(spaceFound.getDescription());
            spaceGetResponse.setLogo(spaceFound.getLogo());
            logger.info("DynamoSpaceServiceImpl-Get successful");
            return new FuncResponse<>(spaceGetResponse);
        }
        catch (Exception e) {
            logger.error("DynamoSpaceServiceImpl-Get failed: {}", e.getMessage());
            return new FuncResponse<>("Failed to get space");
        }
    }

    @Override
    public FuncResponse<SpaceListResponse> listSpace(SpaceListRequest listRequest) {
        logger.info("Start DynamoSpaceServiceImpl-List w. data: {}", listRequest.toJsonSerialized());
        try {
            var partitionKey = _GetSpacePartitionKey(listRequest.getUsername());
            var sortKey = _GetSpaceSortKey(listRequest.getUsername(), listRequest.getName());
            var startKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var query = QueryEnhancedRequest.builder()
                    .limit((int) listRequest.getPageSize())
                    .queryConditional(QueryConditional.sortGreaterThanOrEqualTo(startKey))
                    .build();
            var results = dynamoSpaceServiceDynamoDbTable
                    .query(query)
                    .items()
                    .iterator();
            var spaceList = new ArrayList<SpaceGetResponse>();
            SpaceGetResponse spaceDetail;
            while (results.hasNext()) {
                spaceDetail = new SpaceGetResponse();
                var record = results.next();
                spaceDetail.setUsername(record.getUsername());
                spaceDetail.setName(record.getName());
                spaceDetail.setLogo(record.getLogo());
                spaceDetail.setColor(record.getColor());
                spaceDetail.setDescription(record.getDescription());
                spaceList.add(spaceDetail);
            }
            var response = new SpaceListResponse(spaceList);
            logger.info("DynamoSpaceServiceImpl-List Success");
            return new FuncResponse<>(response);
        }
        catch (Exception e) {
            logger.error("DynamoSpaceServiceImpl-List Failed: {}", e.getMessage());
            return new FuncResponse<>("Failed to list spaces");
        }
    }

    @Override
    public FuncResponse<SpaceUpdateResponse> updateSpace(SpaceUpdateRequest updateRequest) {
        logger.info("Start DynamoSpaceServiceImpl-Update w. data: {}", updateRequest.toJsonSerialized());
        try {
            var partitionKey = _GetSpacePartitionKey(updateRequest.getUsername());
            var sortKey = _GetSpaceSortKey(updateRequest.getUsername(), updateRequest.getName());
            var spaceKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var spaceFound = dynamoSpaceServiceDynamoDbTable.getItem(spaceKey);
            if (Objects.isNull(spaceFound)) {
                throw new Exception("space not found");
            }
            spaceFound.setLogo(updateRequest.getLogo());
            spaceFound.setDescription(updateRequest.getDescription());
            spaceFound.setColor(updateRequest.getColor());
            dynamoSpaceServiceDynamoDbTable.updateItem(spaceFound);
            var spaceUpdateResponse = new SpaceUpdateResponse("Space Updated!");
            logger.info("DynamoSpaceServiceImpl-Update successful");
            return new FuncResponse<>(spaceUpdateResponse);
        }
        catch (Exception e) {
            logger.error("DynamoSpaceServiceImpl-Update failed: {}", e.getMessage());
            return new FuncResponse<>("DynamoSpaceServiceImpl-Update failed");
        }
    }

    @Override
    public FuncResponse<SpaceDeleteResponse> deleteSpace(SpaceDeleteRequest deleteRequest) {
        logger.info("Start DynamoSpaceServiceImpl-Delete w. data: {}", deleteRequest.toJsonSerialized());
        try {
            var partitionKey = _GetSpacePartitionKey(deleteRequest.getUsername());
            var sortKey = _GetSpaceSortKey(deleteRequest.getUsername(), deleteRequest.getName());
            var spaceKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var spaceFound = dynamoSpaceServiceDynamoDbTable.getItem(spaceKey);
            if (Objects.isNull(spaceFound)) {
                throw new Exception("space not found");
            }
            dynamoSpaceServiceDynamoDbTable.deleteItem(spaceKey);
            var spaceDeleteResult = new SpaceDeleteResponse("Deleted Space!");
            logger.info("DynamoSpaceServiceImpl-Delete successful");
            return new FuncResponse<>(spaceDeleteResult);
        }
        catch (Exception e) {
            logger.error("DynamoSpaceServiceImpl-Delete failed: {}", e.getMessage());
            return new FuncResponse<>("DynamoSpaceServiceImpl-Delete failed");
        }
    }

    private String _GetSpacePartitionKey(String username) {
        return String.format("User_%s.Spaces", username);
    }

    private String _GetSpaceSortKey(String username, String name) {
        return String.format("User_%s.Space_%s", username, name);
    }
}
