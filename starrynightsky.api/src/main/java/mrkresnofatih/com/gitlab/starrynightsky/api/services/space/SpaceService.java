package mrkresnofatih.com.gitlab.starrynightsky.api.services.space;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.space.*;

public interface SpaceService {
    FuncResponse<SpaceCreateResponse> createSpace(SpaceCreateRequest createRequest);

    FuncResponse<SpaceGetResponse> getSpace(SpaceGetRequest getRequest);

    FuncResponse<SpaceListResponse> listSpace(SpaceListRequest listRequest);

    FuncResponse<SpaceUpdateResponse> updateSpace(SpaceUpdateRequest updateRequest);

    FuncResponse<SpaceDeleteResponse> deleteSpace(SpaceDeleteRequest deleteRequest);
}
