package mrkresnofatih.com.gitlab.starrynightsky.api.services.user;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;

import java.util.Objects;

@Service
public class DynamoUserServiceImpl implements UserService {
    private final Logger logger;
    private final DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable;

    @Autowired
    public DynamoUserServiceImpl(DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable) {
        this.dynamoUserEntityDynamoDbTable = dynamoUserEntityDynamoDbTable;
        this.logger = LoggerFactory.getLogger(DynamoUserServiceImpl.class);
    }

    @Override
    public FuncResponse<UserCreateResponse> createUser(UserCreateRequest createRequest) {
        logger.info("Start DynamoUserServiceImpl-Create w. data {}", createRequest.toJsonSerialized());
        try {
            var newUser = new DynamoUserEntityImpl();
            newUser.setUsername(createRequest.getUsername());
            newUser.setAvatar(createRequest.getAvatar());
            newUser.setPassword(createRequest.getPassword());
            newUser.setPartitionKey(_GetUserPartitionKey());
            newUser.setSortKey(_GetUserSortKey(createRequest.getUsername()));

            dynamoUserEntityDynamoDbTable.putItem(newUser);
            var newUserResponse = new UserCreateResponse(
                    createRequest.getUsername(),
                    createRequest.getAvatar()
            );
            logger.info("DynamoUserServiceImpl-Create successful");
            return new FuncResponse<>(newUserResponse);
        }
        catch (Exception e) {
            logger.error("DynamoUserServiceImpl-Create failed! {}", e.getMessage());
            return new FuncResponse<>("DynamoUserServiceImpl-Create failed");
        }
    }

    @Override
    public FuncResponse<UserGetResponse> getUser(UserGetRequest getRequest) {
        logger.info("Start DynamoUserServiceImpl-Get w. data {}", getRequest.toJsonSerialized());
        try {
            var partitionKey = _GetUserPartitionKey();
            var sortKey = _GetUserSortKey(getRequest.getUsername());
            var userKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var userFound = dynamoUserEntityDynamoDbTable.getItem(userKey);
            if (Objects.isNull(userFound)) {
                throw new Exception("user not found");
            }
            var userGetResponse = new UserGetResponse(
                    userFound.getUsername(),
                    userFound.getAvatar(),
                    userFound.getPassword()
            );
            logger.info("DynamoUserServiceImpl-Get successful");
            return new FuncResponse<>(userGetResponse);
        }
        catch (Exception e) {
            logger.error("DynamoUserServiceImpl-Get failed! {}", e.getMessage());
            return new FuncResponse<>("DynamoUserServiceImpl-Get failed");
        }
    }

    @Override
    public FuncResponse<UserDeleteResponse> deleteUser(UserDeleteRequest deleteRequest) {
        logger.info("Start DynamoUserServiceImpl-Delete w. data {}", deleteRequest.toJsonSerialized());
        try {
            var partitionKey = _GetUserPartitionKey();
            var sortKey = _GetUserSortKey(deleteRequest.getUsername());
            var userKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var userFound = dynamoUserEntityDynamoDbTable.getItem(userKey);
            if (Objects.isNull(userFound)) {
                throw new Exception("user not found");
            }
            dynamoUserEntityDynamoDbTable.deleteItem(userKey);
            var userDeleteResponse = new UserDeleteResponse();
            logger.info("DynamoUserServiceImpl-Delete successful");
            return new FuncResponse<>(userDeleteResponse);
        }
        catch (Exception e) {
            logger.error("DynamoUserServiceImpl-Delete failed! {}", e.getMessage());
            return new FuncResponse<>("DynamoUserServiceImpl-Delete failed");
        }
    }

    @Override
    public FuncResponse<UserUpdateResponse> updateUser(UserUpdateRequest updateRequest) {
        logger.info("Start DynamoUserServiceImpl-Update w. data {}", updateRequest.toJsonSerialized());
        try {
            var partitionKey = _GetUserPartitionKey();
            var sortKey = _GetUserSortKey(updateRequest.getUsername());
            var userKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var userFound = dynamoUserEntityDynamoDbTable.getItem(userKey);
            if (Objects.isNull(userFound)) {
                throw new Exception("user not found");
            }
            userFound.setAvatar(updateRequest.getAvatar());
            userFound.setPassword(updateRequest.getPassword());
            dynamoUserEntityDynamoDbTable.updateItem(userFound);
            var userUpdateResponse = new UserUpdateResponse(
                    userFound.getUsername(),
                    userFound.getAvatar()
            );
            logger.info("DynamoUserServiceImpl-Update successful");
            return new FuncResponse<>(userUpdateResponse);
        }
        catch (Exception e) {
            logger.error("DynamoUserServiceImpl-Delete failed! {}", e.getMessage());
            return new FuncResponse<>("DynamoUserServiceImpl-Delete failed");
        }
    }

    private String _GetUserSortKey(String username) {
        return String.format("User_%s", username);
    }

    private String _GetUserPartitionKey() {
        return "Users";
    }
}
