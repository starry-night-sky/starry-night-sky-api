package mrkresnofatih.com.gitlab.starrynightsky.api.services.user;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.*;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.*;

public interface UserService {
    FuncResponse<UserCreateResponse> createUser(UserCreateRequest createRequest);

    FuncResponse<UserGetResponse> getUser(UserGetRequest getRequest);

    FuncResponse<UserDeleteResponse> deleteUser(UserDeleteRequest deleteRequest);

    FuncResponse<UserUpdateResponse> updateUser(UserUpdateRequest updateRequest);
}
