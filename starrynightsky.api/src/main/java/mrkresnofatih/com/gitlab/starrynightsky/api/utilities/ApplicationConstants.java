package mrkresnofatih.com.gitlab.starrynightsky.api.utilities;

public class ApplicationConstants {
    public static class UserJwtToken {
        public static final String UsernameClaim = "X-Username";
        public static final String AvatarClaim = "X-Avatar";
    }
}
