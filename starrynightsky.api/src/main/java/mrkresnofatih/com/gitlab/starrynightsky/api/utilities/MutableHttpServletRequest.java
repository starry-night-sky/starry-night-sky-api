package mrkresnofatih.com.gitlab.starrynightsky.api.utilities;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

import java.util.*;

/**
 * A copy from StackOverflow to add custom/additional header key-value pairs.
 * References: https://stackoverflow.com/a/48254071 and https://stackoverflow.com/a/58873149
 */
public class MutableHttpServletRequest extends HttpServletRequestWrapper {
    private final Map<String, String> additionalHeaders;

    public MutableHttpServletRequest(HttpServletRequest request) {
        super(request);
        this.additionalHeaders = new HashMap<>();
    }

    public void putHeader(String key, String value) {
        this.additionalHeaders.put(key, value);
    }

    public String getHeader(String name) {
        var additionalHeaderValue = additionalHeaders.get(name);
        if (additionalHeaderValue != null) {
            return additionalHeaderValue;
        }
        return ((HttpServletRequest) getRequest()).getHeader(name);
    }

    public Enumeration<String> getHeaderNames() {
        // create a set of the custom header names
        Set<String> set = new HashSet<>(additionalHeaders.keySet());

        // now add the headers from the wrapped request object
        @SuppressWarnings("unchecked")
        Enumeration<String> e = ((HttpServletRequest) getRequest()).getHeaderNames();
        while (e.hasMoreElements()) {
            // add the names of the request headers into the list
            String n = e.nextElement();
            set.add(n);
        }

        // create an enumeration from the set and return
        return Collections.enumeration(set);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        Set<String> headerValues = new HashSet<>();
        headerValues.add(this.additionalHeaders.get(name));

        Enumeration<String> underlyingHeaderValues = ((HttpServletRequest) getRequest()).getHeaders(name);
        while (underlyingHeaderValues.hasMoreElements()) {
            headerValues.add(underlyingHeaderValues.nextElement());
        }

        return Collections.enumeration(headerValues);
    }
}
