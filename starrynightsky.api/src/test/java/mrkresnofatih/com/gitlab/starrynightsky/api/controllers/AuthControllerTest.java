package mrkresnofatih.com.gitlab.starrynightsky.api.controllers;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.auth.AuthService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AuthControllerTest {
    @Mock
    private AuthService authService;

    private AuthController authController;

    @BeforeEach
    void setUp() {
        authController = new AuthController(authService);
    }

    FuncResponse<AuthLoginResponse> _get_login_failed_response() {
        var response = new FuncResponse<AuthLoginResponse>();
        response.setData(null);
        response.setErrorMessage("login failed");
        return response;
    }

    FuncResponse<AuthSignupResponse> _get_signup_failed_response() {
        var response = new FuncResponse<AuthSignupResponse>();
        response.setData(null);
        response.setErrorMessage("signup failed");
        return response;
    }

    @Test
    void authLogin_when_auth_service_return_error_should_return_error() {
        var authLoginRequest = new AuthLoginRequest(
                "username",
                "password"
        );
        Mockito.when(authService.authLogin(Mockito.any(AuthLoginRequest.class)))
                .thenReturn(_get_login_failed_response());
        var loginResult = authController.authLogin(authLoginRequest);
        Assertions.assertThat(loginResult.isError()).isTrue();
    }

    @Test
    void authSignup_when_auth_service_return_error_should_return_error() {
        var authSignupRequest = new AuthSignupRequest(
                "username",
                "avatar",
                "password"
        );
        Mockito.when(authService.authSignup(Mockito.any(AuthSignupRequest.class)))
                .thenReturn(_get_signup_failed_response());
        var signupResult = authController.authSignup(authSignupRequest);
        Assertions.assertThat(signupResult.isError()).isTrue();
    }
}