package mrkresnofatih.com.gitlab.starrynightsky.api.services.auth;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthLoginRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.auth.AuthSignupRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserCreateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserCreateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserGetResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.jwt.JwtService;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.user.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class SimpleAuthServiceImplTest {
    @Mock
    private UserService userService;
    @Mock
    private JwtService jwtService;
    private SimpleAuthServiceImpl simpleAuthService;

    @BeforeEach
    void setUp() {
        simpleAuthService = new SimpleAuthServiceImpl(userService, jwtService);
    }

    @Test
    void login_when_invalid_credentials_should_return_error() {
        var incorrectLoginRequest = new AuthLoginRequest(
                "username",
                "password"
        );
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(new UserGetResponse(
                        "username",
                        "avatar",
                        "password123#"
                )));
        var loginResult = simpleAuthService.authLogin(incorrectLoginRequest);
        Assertions.assertThat(loginResult.isError()).isTrue();
    }

    @Test
    void login_when_valid_credentials_should_return_response() {
        var correctLoginRequest = new AuthLoginRequest(
                "username",
                "password123#"
        );
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(new UserGetResponse(
                        "username",
                        "avatar",
                        "password123#"
                )));
        Mockito.when(jwtService.createJwt(Mockito.any(JwtCreateRequest.class)))
                .thenReturn(new FuncResponse<>(new JwtCreateResponse("some-token")));
        var loginResult = simpleAuthService.authLogin(correctLoginRequest);
        Assertions.assertThat(loginResult.isError()).isFalse();
    }

    @Test
    void login_when_user_not_found_should_return_error() {
        var nonExistentUserLoginRequest = new AuthLoginRequest(
                "username",
                "password123#"
        );
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>("User Not Found"));
        var loginResult = simpleAuthService.authLogin(nonExistentUserLoginRequest);
        Assertions.assertThat(loginResult.isError()).isTrue();
    }

    @Test
    void login_when_generate_token_fail_should_return_error() {
        var correctLoginRequest = new AuthLoginRequest(
                "username",
                "password123#"
        );
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(new UserGetResponse(
                        "username",
                        "avatar",
                        "password123#"
                )));
        Mockito.when(jwtService.createJwt(Mockito.any(JwtCreateRequest.class)))
                .thenReturn(new FuncResponse<>("Failed To Generate JWT Token"));
        var loginResult = simpleAuthService.authLogin(correctLoginRequest);
        Assertions.assertThat(loginResult.isError()).isTrue();
    }

    @Test
    void signup_when_username_taken_should_return_error() {
        var userTakenSignupRequest = new AuthSignupRequest(
                "username",
                "avatar",
                "password"
        );
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(new UserGetResponse(
                        "username",
                        "avatar",
                        "password"
                )));
        var signupResult = simpleAuthService.authSignup(userTakenSignupRequest);
        Assertions.assertThat(signupResult.isError()).isTrue();
    }

    @Test
    void signup_when_username_available_should_return_response() {
        var userAvailableSignUpRequest = new AuthSignupRequest(
                "username",
                "avatar",
                "password"
        );
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>("User Not Found"));
        Mockito.when(userService.createUser(Mockito.any(UserCreateRequest.class)))
                .thenReturn(new FuncResponse<>(new UserCreateResponse(
                        "username",
                        "avatar"
                )));
        var signupResult = simpleAuthService.authSignup(userAvailableSignUpRequest);
        Mockito.verify(userService, Mockito.times(1)).createUser(Mockito.any(UserCreateRequest.class));
        Assertions.assertThat(signupResult.isError()).isFalse();
    }
}