package mrkresnofatih.com.gitlab.starrynightsky.api.services.jwt;

import mrkresnofatih.com.gitlab.starrynightsky.api.configurations.StarryNightSkyConfig;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.jwt.JwtValidateRequest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

class Auth0JwtServiceImplTest {
    private Auth0JwtServiceImpl jwtService;

    StarryNightSkyConfig _get_mock_starry_night_sky_config() {
        return new StarryNightSkyConfig("fvzFFtc2OzW7gyahbfTWw6gsIugAJt0D5jByge1Z", "https://app.9UdItBPLFZITWGxGjL2m.com");
    }

    @BeforeEach
    void setUp() {
        var config = _get_mock_starry_night_sky_config();
        jwtService = new Auth0JwtServiceImpl(config);
    }

    Map<String, String> _getCustomClaims() {
        var claims = new HashMap<String, String>();
        claims.put("my_1_claim_key", "my_1_claim_value");
        claims.put("my_2_claim_key", "my_2_claim_value");
        claims.put("my_3_claim_key", "my_3_claim_value");
        return claims;
    }

    @Test
    void createJwt_when_sign_throws_should_return_error() {
        var claims = _getCustomClaims();
        var jwtCreateReq = new JwtCreateRequest(
                claims,
                5L
        );
        var createJwtResult = jwtService.createJwt(jwtCreateReq);
        Assertions.assertThat(createJwtResult.isError()).isFalse();
    }

    @Test
    void validateJwt_when_inserted_expired_token_should_return_error() {
        var claims = _getCustomClaims();
        var jwtCreateReq = new JwtCreateRequest(
                claims,
                3L
        );
        var createJwtResult = jwtService.createJwt(jwtCreateReq);
        var jwtValidateReq = new JwtValidateRequest(
                claims.keySet(),
                createJwtResult.getData().getToken()
        );
        try {
            Thread.sleep(5000L);
        } catch (Exception ignored) {
        }
        var validateJwtResult = jwtService.validateJwt(jwtValidateReq);
        Assertions.assertThat(validateJwtResult.isError()).isTrue();
    }

    @Test
    void validateJwt_when_inserted_unexpired_token_should_return_error() {
        var claims = _getCustomClaims();
        var jwtCreateReq = new JwtCreateRequest(
                claims,
                5L
        );
        var createJwtResult = jwtService.createJwt(jwtCreateReq);
        var jwtValidateReq = new JwtValidateRequest(
                claims.keySet(),
                createJwtResult.getData().getToken()
        );
        try {
            Thread.sleep(2000L);
        } catch (Exception ignored) {
        }
        var validateJwtResult = jwtService.validateJwt(jwtValidateReq);
        Assertions.assertThat(validateJwtResult.isError()).isFalse();
    }
}