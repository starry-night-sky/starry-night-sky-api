package mrkresnofatih.com.gitlab.starrynightsky.api.services.profile;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.FuncResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserGetRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserGetResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserUpdateRequest;
import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.UserUpdateResponse;
import mrkresnofatih.com.gitlab.starrynightsky.api.services.user.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SimpleProfileServiceImplTest {
    @Mock
    private UserService userService;
    private SimpleProfileServiceImpl simpleProfileService;

    @BeforeEach
    void setUp() {
        simpleProfileService = new SimpleProfileServiceImpl(userService);
    }

    @Test
    void get_when_user_service_get_user_failed_should_return_error() {
        var getReq = new ProfileGetRequest("username");
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>("Failed to get user"));
        var getResult = simpleProfileService.get(getReq);
        Mockito.verify(userService, Mockito.times(1))
                .getUser(Mockito.any(UserGetRequest.class));
        Assertions.assertThat(getResult.isError()).isTrue();
    }

    @Test
    void get_when_user_service_get_user_success_should_return_success() {
        var getReq = new ProfileGetRequest("username");
        Mockito.when(userService.getUser(Mockito.any(UserGetRequest.class)))
            .thenReturn(new FuncResponse<>(new UserGetResponse("username", "avatar", "password")));
        var getResult = simpleProfileService.get(getReq);
        Mockito.verify(userService, Mockito.times(1))
                .getUser(Mockito.any(UserGetRequest.class));
        Assertions.assertThat(getResult.isError()).isFalse();
    }

    @Test
    void update_when_user_service_update_user_failed_should_return_error() {
        var updateReq = new ProfileUpdateRequest(
                "username",
                "avatar",
                "password"
        );
        Mockito.when(userService.updateUser(Mockito.any(UserUpdateRequest.class)))
                .thenReturn(new FuncResponse<>("Failed to find user"));
        var updateResult = simpleProfileService.update(updateReq);
        Mockito.verify(userService, Mockito.times(1))
                .updateUser(Mockito.any(UserUpdateRequest.class));
        Assertions.assertThat(updateResult.isError()).isTrue();
    }

    @Test
    void update_when_user_service_update_user_success_should_return_success() {
        var updateReq = new ProfileUpdateRequest(
                "username",
                "avatar",
                "password"
        );
        Mockito.when(userService.updateUser(Mockito.any(UserUpdateRequest.class)))
                .thenReturn(new FuncResponse<>(new UserUpdateResponse(updateReq.getUsername(), updateReq.getAvatar())));
        var updateResult = simpleProfileService.update(updateReq);
        Mockito.verify(userService, Mockito.times(1))
                .updateUser(Mockito.any(UserUpdateRequest.class));
        Assertions.assertThat(updateResult.isError()).isFalse();
    }
}