package mrkresnofatih.com.gitlab.starrynightsky.api.services.space;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.space.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.PageIterable;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class DynamoSpaceServiceImplTest {
    @Mock
    private DynamoDbTable<DynamoSpaceEntityImpl> dynamoSpaceEntityDynamoDbTable;
    private DynamoSpaceServiceImpl dynamoSpaceService;

    @BeforeEach
    void setUp() {
        dynamoSpaceService = new DynamoSpaceServiceImpl(dynamoSpaceEntityDynamoDbTable);
    }

    SpaceCreateRequest _getCreateRequest() {
        return new SpaceCreateRequest(
                "username",
                "spaceName",
                "A random space",
                "#19A7CE",
                "https://myapp.somelogo.png"
        );
    }

    SpaceUpdateRequest _getUpdateRequest() {
        return new SpaceUpdateRequest(
                "username",
                "spaceName",
                "A random space",
                "#19A7CE",
                "https://myapp.somelogo.png"
        );
    }

    SpaceGetRequest _getGetRequest() {
        return new SpaceGetRequest(
                "username",
                "spaceName"
        );
    }

    SpaceListRequest _getListRequest() {
        return new SpaceListRequest(
                "username",
                "name",
                25L
        );
    }

    @Test
    void create_when_put_item_throws_should_return_error() {
        var correctCreateSpaceReq = _getCreateRequest();
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoSpaceEntityDynamoDbTable)
                .putItem(Mockito.any(DynamoSpaceEntityImpl.class));
        var createResult = dynamoSpaceService.createSpace(correctCreateSpaceReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                        .putItem(Mockito.any(DynamoSpaceEntityImpl.class));
        Assertions.assertThat(createResult.isError()).isTrue();
    }

    @Test
    void create_when_put_item_success_should_return_success() {
        var createReq = _getCreateRequest();
        var createResult = dynamoSpaceService.createSpace(createReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                .putItem(Mockito.any(DynamoSpaceEntityImpl.class));
        Assertions.assertThat(createResult.isError()).isFalse();
    }

    @Test
    void get_when_get_item_returns_null_should_return_error() {
        var getReq = _getGetRequest();
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var getResult = dynamoSpaceService.getSpace(getReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                        .getItem(Mockito.any(Key.class));
        Assertions.assertThat(getResult.isError()).isTrue();
    }

    @Test
    void get_when_get_item_returns_non_null_should_return_success() {
        var getReq = _getGetRequest();
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoSpaceEntityImpl(
                        "partitionKey",
                        "sortKey",
                        "username",
                        "spaceName",
                        "A space",
                        "#FFFFFF",
                        "https://app.somelogo.png"
                ));
        var getResult = dynamoSpaceService.getSpace(getReq);
        Assertions.assertThat(getResult.isError()).isFalse();
    }

    @Test
    void delete_when_get_item_return_null_should_return_error() {
        var deleteReq = new SpaceDeleteRequest(
                "username",
                "spaceName"
        );
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var deleteResult = dynamoSpaceService.deleteSpace(deleteReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                .getItem(Mockito.any(Key.class));
        Assertions.assertThat(deleteResult.isError()).isTrue();
    }

    @Test
    void delete_when_delete_item_throw_should_return_error() {
        var deleteReq = new SpaceDeleteRequest(
                "username",
                "spaceName"
        );
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoSpaceEntityImpl());
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoSpaceEntityDynamoDbTable)
                .deleteItem(Mockito.any(Key.class));
        var deleteResult = dynamoSpaceService.deleteSpace(deleteReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                .deleteItem(Mockito.any(Key.class));
        Assertions.assertThat(deleteResult.isError()).isTrue();
    }

    @Test
    void delete_when_delete_item_success_should_return_success() {
        var deleteReq = new SpaceDeleteRequest(
                "username",
                "spaceName"
        );
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoSpaceEntityImpl());
        var deleteResult = dynamoSpaceService.deleteSpace(deleteReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                .deleteItem(Mockito.any(Key.class));
        Assertions.assertThat(deleteResult.isError()).isFalse();
    }

    @Test
    void update_when_get_item_return_null_should_return_failed() {
        var updateReq = _getUpdateRequest();
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var updateResult = dynamoSpaceService.updateSpace(updateReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                .getItem(Mockito.any(Key.class));
        Assertions.assertThat(updateResult.isError()).isTrue();
    }

    @Test
    void update_when_update_item_throws_should_return_failed() {
        var updateReq = _getUpdateRequest();
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoSpaceEntityImpl());
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoSpaceEntityDynamoDbTable)
                .updateItem(Mockito.any(DynamoSpaceEntityImpl.class));
        var updateResult = dynamoSpaceService.updateSpace(updateReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                        .updateItem(Mockito.any(DynamoSpaceEntityImpl.class));
        Assertions.assertThat(updateResult.isError()).isTrue();
    }

    @Test
    void update_when_update_item_success_should_return_success() {
        var updateReq = _getUpdateRequest();
        Mockito.when(dynamoSpaceEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoSpaceEntityImpl());
        Mockito.when(dynamoSpaceEntityDynamoDbTable.updateItem(Mockito.any(DynamoSpaceEntityImpl.class)))
                .thenReturn(new DynamoSpaceEntityImpl());
        var updateResult = dynamoSpaceService.updateSpace(updateReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                .updateItem(Mockito.any(DynamoSpaceEntityImpl.class));
        Assertions.assertThat(updateResult.isError()).isFalse();
    }

    @Test
    void list_when_query_throws_should_return_error() {
        var listReq = _getListRequest();
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoSpaceEntityDynamoDbTable)
                .query(Mockito.any(QueryEnhancedRequest.class));
        var listResult = dynamoSpaceService.listSpace(listReq);
        Mockito.verify(dynamoSpaceEntityDynamoDbTable, Mockito.times(1))
                .query(Mockito.any(QueryEnhancedRequest.class));
        Assertions.assertThat(listResult.isError()).isTrue();
    }
}