package mrkresnofatih.com.gitlab.starrynightsky.api.services.user;

import mrkresnofatih.com.gitlab.starrynightsky.api.models.user.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;

@ExtendWith(MockitoExtension.class)
class DynamoUserServiceImplTest {
    @Mock
    private DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable;
    private DynamoUserServiceImpl dynamoUserService;

    @BeforeEach
    void setUp() {
        dynamoUserService = new DynamoUserServiceImpl(dynamoUserEntityDynamoDbTable);
    }

    @Test
    void create_when_correct_input_should_call_put_item_once() {
        var correctCreateUserRequest = new UserCreateRequest(
                "username",
                "avatar",
                "password"
        );
        dynamoUserService.createUser(correctCreateUserRequest);
        Mockito.verify(dynamoUserEntityDynamoDbTable, Mockito.times(1))
                .putItem(Mockito.any(DynamoUserEntityImpl.class));
    }

    @Test
    void create_when_put_item_throw_should_return_error() {
        var correctCreateUserRequest = new UserCreateRequest(
                "username",
                "avatar",
                "password"
        );
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoUserEntityDynamoDbTable)
                .putItem(Mockito.any(DynamoUserEntityImpl.class));
        var createResult = dynamoUserService.createUser(correctCreateUserRequest);
        Assertions.assertThat(createResult.isError()).isTrue();
    }

    @Test
    void get_when_get_item_returns_null_should_return_error() {
        var correctGetUserRequest = new UserGetRequest(
                "username"
        );
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var getResult = dynamoUserService.getUser(correctGetUserRequest);
        Assertions.assertThat(getResult.isError()).isTrue();
    }

    @Test
    void get_when_get_item_throw_should_return_error() {
        var correctGetUserReq = new UserGetRequest(
                "username"
        );
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoUserEntityDynamoDbTable)
                .getItem(Mockito.any(Key.class));
        var getResult = dynamoUserService.getUser(correctGetUserReq);
        Assertions.assertThat(getResult.isError()).isTrue();
    }

    DynamoUserEntityImpl _get_sample_user_return() {
        var returnedDynamoUser = new DynamoUserEntityImpl();
        returnedDynamoUser.setPartitionKey("Users");
        returnedDynamoUser.setSortKey("User_username");
        returnedDynamoUser.setAvatar("avatar");
        returnedDynamoUser.setUsername("username");
        returnedDynamoUser.setPassword("password");
        return returnedDynamoUser;
    }

    @Test
    void get_when_get_item_success_should_return_data() {
        var correctGetUserReq = new UserGetRequest(
                "username"
        );
        var returnedDynamoUser = _get_sample_user_return();
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(returnedDynamoUser);
        var getResult = dynamoUserService.getUser(correctGetUserReq);
        Assertions.assertThat(getResult.isError()).isFalse();
    }

    @Test
    void delete_when_get_item_return_null_should_return_error() {
        var correctDeleteRequest = new UserDeleteRequest(
                "username"
        );
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var deleteResult = dynamoUserService.deleteUser(correctDeleteRequest);
        Assertions.assertThat(deleteResult.isError()).isTrue();
    }

    @Test
    void delete_when_get_item_return_non_null_and_delete_throw_should_return_error() {
        var correctDeleteRequest = new UserDeleteRequest(
                "username"
        );
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoUserEntityImpl());
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoUserEntityDynamoDbTable)
                .deleteItem(Mockito.any(Key.class));
        var deleteResult = dynamoUserService.deleteUser(correctDeleteRequest);
        Assertions.assertThat(deleteResult.isError()).isTrue();
    }

    @Test
    void delete_when_get_item_return_non_null_and_delete_return_data_should_return_error_false() {
        var correctDeleteRequest = new UserDeleteRequest(
                "username"
        );
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoUserEntityImpl());
        Mockito.when(dynamoUserEntityDynamoDbTable.deleteItem(Mockito.any(Key.class)))
                .thenReturn(new DynamoUserEntityImpl());
        var deleteResult = dynamoUserService.deleteUser(correctDeleteRequest);
        Assertions.assertThat(deleteResult.isError()).isFalse();
        Assertions.assertThat(deleteResult.getData()).isNotNull();
    }

    @Test
    void update_when_get_item_return_null_should_return_error() {
        var updateRequest = new UserUpdateRequest(
                "username",
                "avatar",
                "password"
        );
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var updateResult = dynamoUserService.updateUser(updateRequest);
        Assertions.assertThat(updateResult.isError()).isTrue();
    }

    @Test
    void update_when_get_item_return_data_and_update_item_success_should_return_error_false() {
        var updateRequest = new UserUpdateRequest(
                "username",
                "avatar",
                "password"
        );
        var getUpdateResponse = _get_sample_user_return();
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(getUpdateResponse);
        Mockito.when(dynamoUserEntityDynamoDbTable.updateItem(Mockito.any(DynamoUserEntityImpl.class)))
                .thenReturn(getUpdateResponse);
        var updateResult = dynamoUserService.updateUser(updateRequest);
        Assertions.assertThat(updateResult.isError()).isFalse();
    }
}